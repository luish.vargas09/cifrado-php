<?php 
require 'Cifrado.php';

$texto = 'Chanchito Feliz';

$opciones = [
    'password' => 'ABCD1234',
    'metodo' => 'AES-256-CBC',
];

echo 'Texto original: ' . $texto;
echo '<br>';
echo '<br>';
echo 'Texto cifrado: ' . cifrado($texto, $opciones);
echo '<br>';
echo '<br>';
echo 'Texto descifrado: ' . descifrar(cifrado($texto, $opciones), $opciones);