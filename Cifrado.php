<?php 
/**
 * Encrypts the given data using the specified encryption method and password.
 *
 * @param string $datos The data to be encrypted.
 * @param array $opciones (Optional) An array containing the password and encryption method.
 * @return string The encrypted data encoded in base64.
 */
function cifrado($datos, $opciones = []) {
    
    // Get the password and encryption method from the options array
    $password = $opciones['password'] ?? '';
    $metodo = $opciones['metodo'] ?? '';

    // Get the size of the initialization vector (IV) for the encryption method
    $ivSize = openssl_cipher_iv_length($metodo);
    
    // Generate a random initialization vector (IV)
    $iv = openssl_random_pseudo_bytes($ivSize);
    
    // Encrypt the data using the specified encryption method, password, and IV
    $cifrado = openssl_encrypt($datos, $metodo, $password, OPENSSL_RAW_DATA, $iv);
    
    // Return the encrypted data encoded in base64
    return base64_encode($iv . $cifrado);
}

/**
 * Decrypts the given encrypted data using the specified encryption method and password.
 *
 * @param string $datos The encrypted data to be decrypted.
 * @param array $opciones (Optional) An array containing the password and encryption method.
 * @return string The decrypted data.
 */
function descifrar($datos, $opciones = []) {
    
    // Get the password and encryption method from the options array
    $password = $opciones['password'] ?? '';
    $metodo = $opciones['metodo'] ?? '';
    
    // Decode the base64-encoded data
    $datos = base64_decode($datos);
    
    // Get the size of the initialization vector (IV) for the encryption method
    $ivSize = openssl_cipher_iv_length($metodo);
    
    // Extract the initialization vector (IV) from the encrypted data
    $iv = substr($datos, 0, $ivSize);
    
    // Extract the encrypted data from the encrypted data
    $cifrado = substr($datos, $ivSize);
    
    // Decrypt the encrypted data using the specified encryption method, password, and IV
    return openssl_decrypt($cifrado, $metodo, $password, OPENSSL_RAW_DATA, $iv);
}

